﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{
    class DefaultProduct:IProduct
    {
        public String ShipFrom()
        {
            return "Not available";
        }
    }
}
